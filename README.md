# Scraper for European Commission feedback

This R script scrapes all relevant public data from feedback periods opened by the European Commission.

It is hardcoded to the [feedback on the Radio Equipment Directive's Article 3(3)(i)](https://ec.europa.eu/info/law/better-regulation/initiatives/ares-2018-6621038_en). It may also work with other feedback and consultation forms if you changed the URL and the page count. However, this hasn't been tested.

All results are stored in `feedback.csv`. The attachments in the `attachments` directory.

## License

GNU GPL 3.0
